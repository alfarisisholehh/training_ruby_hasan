class CreateColumns < ActiveRecord::Migration
  def change
    	add_column :articles, :rating, :integer
	change_column :users, :address, :text
	rename_column :articles, :body, :descriptions
  end
end
