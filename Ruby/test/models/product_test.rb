require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  def test_relation_between_products_and_user
		user=User.create(:id=>"12",:first_name=>"First",:last_name=>"Last",:email=>"first@last.com",:password=>"test")
		assert_equal user.valid?,true
		product=Product.create(:name=>"Milk",:price=>"1000",:description=>"This is milk",:user_id=>user.id)
		assert_not_nil product
		assert_equal product.user.full_name,"First Last"
	end

	def test_red_product
		product=Product.create(:name=>"Jeans red blue",:description=>"This is milk")
		assert_not_nil product
		assert_equal Product.count,1
	end
end
