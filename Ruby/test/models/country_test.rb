require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  def test_relation_between_country_and_user
		country=Country.create(:code=>"id",:name=>"Indonesia")
		assert_not_nil country
		user=User.new(:id=>"10",:first_name=>"First",:last_name=>"Last",:email=>"first@last.com",:password=>"test",:country_id=>country.id)
		assert_equal user.valid?,true
		user.save
		assert_equal Country.find(user.country_id).name,"Indonesia"
	end
end
