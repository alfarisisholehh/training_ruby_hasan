require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def test_relation_between_category_and_product
  	category = Category.create(:name=>"Test")
  	assert_not_nil category
  	product = Product.create(:name=>"Water")
  	assert_not_nil category
  	ProductCategory.create(:product_id=>product.id, :category_id=>category.id)
  	assert_equal category.product.count,1
  end
end
