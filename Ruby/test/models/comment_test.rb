require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  def test_relation_between_article_and_comment
 	article = Article.create(:title => "title", :description =>"this is description") 
 	assert_not_nil article
 	comment = Comment.create(:article_id => article.id, :content => "my Content")	
 	assert_not_nil article.comments
 	assert_equal article.comments.empty?, false
 	assert_equal article.comments[0].class, comment
 end
end
