require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
	def test_save_without_title
		article = Article.new(:description => 'body')
		assert_equal article.valid?, false
		assert_equal article.save, false
	end

	def test_with_title
		article = Article.new(:title=>'title')
		assert_equal article.valid?, true
		assert_equal article.save, true
	end

	def test_rating
		Article.create([{:title => "title",:rating=>'1'}])
		assert_not_nil Article.rating(1)
		assert_equal Article.rating(1)[0].rating, 1
	end

	def test_validation_title
		Article.create([{:title => "title"}])
		article=Article.new(:title=>'title')
		assert_equal article.save,false#uniqueness
		article.title=''
		assert_equal article.save,false#can't blank
	end

	def test_relation_between_article_and_comment
		article =Article.create(:title => "title", :description => "body")
		assert_not_nil article
		comment = Comment.create(:article_id => article.id, :content => "my comment")
		assert_not_nil article.comments
		assert_equal article.comments.empty?, false
		assert_equal article.comments[0].class, Comment
	end

	def test_relation_between_article_and_user
		user=User.create(:id=>"5",:first_name=>"First",:last_name=>"Last",:email=>"first@last.com",:password=>"test")
		assert_equal user.valid?,true
		article=user.articles.create(:title=>"title",:user_id=>"5")
		assert_equal user.articles.first.title,"title"
	end
end