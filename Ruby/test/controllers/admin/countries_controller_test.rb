require 'test_helper'

class Admin::CountriesControllerTest < ActionController::TestCase
  def setup
		@country=Country.find(:first)
	end
	
	def test_index
		get :index
		assert_response :success
		assert_not_nil assigns(:country)
	end

	def test_new
		login_as("aaron")
		get :new
		assert_not_nil assigns(:country)
		assert_response :success
	end
	
	def test_create
		login_as("aaron")
		assert_difference('Country.count') do
			post :create, country: {code:"id",name:"Indonesia"}
			assert_not_nil assigns(:country)
			assert_equal assigns(:country).code, "id"
			assert_equal assigns(:country).valid?, true
		end
		assert_response :redirect
		assert_redirected_to admin_countries_path
		assert_equal flash[:notice], 'Country added'
	end
	
	def test_create_with_invalid_parameter
		login_as("aaron")		
		assert_no_difference('Country.count') do
			post :create, country: {code: "nil"}
			assert_not_nil assigns(:country)
			assert_equal assigns(:country).code, "nil"
			assert_equal assigns(:country).valid?,false
		end
		assert_response :success
	end
	
	def test_show
		get :show, :id=>Country.first.id
		assert_not_nil assigns(:country)
		assert_response :success
	end

	def test_edit
		login_as("aaron")
		country=Country.first.id
		get :edit, :id=>country
		assert_not_nil assigns(:country)
		assert_response :success
	end
	
	def test_update
		login_as("aaron")
		put :update, :id=>Country.first.id,
		:country=>{:name=>"America"}
		assert_not_nil assigns(:country)
		assert_equal assigns(:country).name,"America"
		assert_response :redirect
		assert_redirected_to admin_countries_path
		assert_equal flash[:notice], 'Country updated'
	end
	
	def test_destroy
		login_as("aaron")
		Country.create(:code=>"id",:name=>"Testing")
		assert_difference('Country.count', -1) do
			delete :destroy,:id=>Country.find(1)
			assert_not_nil assigns(:country)
		end
		assert_response :redirect
		assert_redirected_to admin_countries_path
		assert_equal flash[:notice], 'Country deleted'
	end
	
	def login_as(first_name)
		user = User.find_by_first_name(first_name)
		@request.session[:user_id] = user.id unless user.nil?
	end
end
