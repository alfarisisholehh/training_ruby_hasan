require 'test_helper'

class Admin::CommentsControllerTest < ActionController::TestCase
 	def setup
		@comment=Comment.find(:first)
	end
	
	def test_index
		get :index
		assert_response :success
		assert_not_nil assigns(:comment)
	end

	def test_create
		login_as("Hasan")
		assert_equal flash[:notice], 'Comment added'
	end

	def test_destroy
		login_as("Hasan")
		assert_difference('Comment.count', -1) do
			delete :destroy,:id=>Comment.first.id
			assert_not_nil assigns(:comment)
		end
		assert_response :redirect
		assert_redirected_to admin_comments_path
		assert_equal flash[:notice], 'Comment deleted'
	end
	
	def login_as(first_name)
		user = User.find_by_first_name(first_name)
		@request.session[:user_id] = user.id unless user.nil?
	end
end
