require 'test_helper'

class Admin::CategoriesControllerTest < ActionController::TestCase
  def setup
		@category=Category.find(:first)
	end
	
	def test_index
		get :index
		assert_response :success
		assert_not_nil assigns(:category)
	end

	def test_new
		login_as("hasan")
		get :new
		assert_not_nil assigns(:category)
		assert_response :success
	end
	
	def test_create
		login_as("hasan")
		assert_difference('Category.count') do
			post :create, category: {name: 'Some test'}
			assert_not_nil assigns(:category)
			assert_equal assigns(:category).name, "Some name"
			assert_equal assigns(:category).valid?, true
		end
		assert_response :redirect
		assert_redirected_to admin_categories_path
		assert_equal flash[:notice], 'categories added'
	end
	
	def test_show
		get :show, :id=>Category.first.id
		assert_not_nil assigns(:category)
		assert_response :success
	end

	def test_edit
		login_as("hasan")
		article=Category.first.id
		get :edit, :id=>category
		assert_not_nil assigns(:category)
		assert_response :success
	end
	
	def test_update
		login_as("hasan")
		put :update, :id=>Category.first.id,
		:article=>{:name => 'updated title'}
		assert_not_nil assigns(:category)
		assert_equal assigns(:category).name, 'updated name'
		assert_response :redirect
		assert_redirected_to admin_categories_path
		assert_equal flash[:notice], 'Category updated'
	end
	
	def test_destroy
		login_as("hasan")
		assert_difference('Category.count', -1) do
			delete :destroy,:id=>Category.first.id
			assert_not_nil assigns(:category)
		end
		assert_response :redirect
		assert_redirected_to admin_categories_path
		assert_equal flash[:notice], 'Category deleted'
	end
	
	def login_as(first_name)
		user = User.find_by_first_name(first_name)
		@request.session[:user_id] = user.id unless user.nil?
	end
end
