require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  def setup
  	@article = Article.find(:first)
  end

  def test_index
  	get :index
  	assert_response :success
  	assert_not_nil assigns(:articles)
  end

  def test_new
  	login_as('martin@kiranatama.com')
  	get :new
  	assert_not_nil assigns(:article)
  	assert_response :success
  end

  def test_create
  	login_as('martin@kiranatama.com')
  	assert_difference('Article.count') do 
  		post :create, :article {title: 'new title'}
  		assert_not_nil assigns(:article)
  		assert_equal assigns(:article).title, "new title"
  		assert_equal assigns(:article).valid?, true
  	end
  	assert_response :redirect
  	assert_redirected_to articles_path
  	assert_equal flash[:notice], 'Post Success'
  end

  def test_create_with_invalid_parameter
  	login_as('martin@kiranatama.com')
  	assert_no_difference('Article.count') do
  	post :create, :article => {:title => nil, :description => nil}
    assert_not_nil assigns(:article)
    assert_equal assigns(:article).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'Post was failed to create.'
  end

  def test_show
    get :show, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_edit
  	login_as('martin@kiranatama.com')
    get :edit, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_update
  	login_as('martin@kiranatama.com')
    put :update, :id => Article.first.id,
                 :article => {:title => 'updated title', :description => "updated body"}
    assert_not_nil assigns(:article)
    assert_equal assigns(:article).title, 'updated title'
    assert_response :redirect
    assert_redirected_to article_path(assigns(:article))
    assert_equal flash[:notice], 'Post was successfully updated.'
  end


  def test_destroy
  	login_as('martin@kiranatama.com')
    assert_difference('Article.count', -1) do
    delete :destroy, :id => Article.first.id
    assert_not_nil assigns(:article)
    end
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:error], 'Post successfully deleted'
  end

  def login_as(first_name)
		user = User.find_by_first_name(first_name)
		@request.session[:user_id] = user.id unless user.nil?
	end
end
