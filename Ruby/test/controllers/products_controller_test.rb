require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  def setup
		@product=Product.find(:first)
	end
	
	def test_index
		get :index
		assert_response :success
		assert_not_nil assigns(:product)
	end

	def test_new
		login_as("hasan")
		get :new
		assert_not_nil assigns(:product)
		assert_response :success
	end
	
	def test_create
		login_as("hasan")
		assert_difference('Product.count') do
			post :create, product: {name: 'Some title'}
			assert_not_nil assigns(:product)
			assert_equal assigns(:product).name, "Some title"
			assert_equal assigns(:product).valid?, true
		end
		assert_response :redirect
		assert_redirected_to products_path
		assert_equal flash[:notice], 'Product added'
	end
	
	def test_show
		get :show, :id=>Product.first.id
		assert_not_nil assigns(:product)
		assert_response :success
	end

	def test_edit
		login_as("hasan")
		article=Product.first.id
		get :edit, :id=>product
		assert_not_nil assigns(:product)
		assert_response :success
	end
	
	def test_update
		login_as("hasan")
		put :update, :id=>Product.first.id,
		:article=>{:name => 'updated title'}
		assert_not_nil assigns(:product)
		assert_equal assigns(:product).name, 'updated title'
		assert_response :redirect
		assert_redirected_to products_path
		assert_equal flash[:notice], 'Product updated'
	end
	
	def test_destroy
		login_as("hasan")
		assert_difference('Product.count', -1) do
			delete :destroy,:id=>Product.first.id
			assert_not_nil assigns(:product)
		end
		assert_response :redirect
		assert_redirected_to products_path
		assert_equal flash[:notice], 'Product deleted'
	end
	
	def login_as(first_name)
		user = User.find_by_first_name(first_name)
		@request.session[:user_id] = user.id unless user.nil?
	end
end
