# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RubyExercise::Application.config.secret_key_base = '8fc09524e3733105b34534124849fe9b2ffebaf378900dc068b82c984f4b44d070137a75f8b185d0dac9a48017a972bb7e7c46a85bf0beb5eb9ea79cbc0b6310'
