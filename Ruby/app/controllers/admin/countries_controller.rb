class Admin::CountriesController < Admin::ApplicationController
  before_filter :require_admin_login
  
  def index
    @country =Country.all
  end
  
  def show
    @country=Country.find(params[:id])
  end

  def new
    @country = Country.new
  end

  def create
    @country=Country.new(country_params)
	  if @country.save
			redirect_to admin_countries_path,:notice=>"Country added"
		else
			render 'new'
		end 
  end

  def destroy
    @country=Country.find(params[:id])
    Country.delete(params[:id])
    redirect_to admin_countries_path, :notice => "Article deleted"
  end

  def edit
    @country=Country.find(params[:id])
  end

    def update
    @country=Country.find(params[:id])
    if @country.update_attributes(country_params)
      redirect_to admin_countries_path,:notice=>"Article updated"
    else
      render 'edit'
    end
  end

  private
  def country_params
    params.require(:country).permit(:code, :name)
  end
end
