class Admin::CommentsController < Admin::ApplicationController
  before_filter :require_admin_login

	def index
		@comment=Comment.all
	end
	
	def show
		@comment=Comment.find(params[:id])
	end
	
	def create
		@comment=Comment.new(comment_params)
		respond_to do |format|
			if @comment.save
				format.html { redirect_to(admin_article_path(article),:notice=> 'Comment added') }
				format.js { @comments = Article.find(params[:comment][:article_id].to_i).comments }
			else
				render 'show'
			end
		end
	end

	def destroy
		@comment=Comment.delete(params[:id])
		redirect_to admin_comments_path,:notice=>"Comment deleted"
	end

	private

  	def comment_params
    	params.require(:comment).permit(:content, :article_id)
 	end
end
