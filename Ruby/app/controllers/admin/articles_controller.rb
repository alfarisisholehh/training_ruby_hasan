class Admin::ArticlesController < Admin::ApplicationController
 	before_filter :require_admin_login
  
  	def create
	    @article=Article.new(article_params)
		if @article.save
			redirect_to admin_articles_path,:notice=>"Article added"
		else
			render 'new'
		end  
  	end

  	def new
    	@article=Article.new
  	end
  
  	def show
		@article=Article.find(params[:id])
	end
  
	def edit
		@article=Article.find(params[:id])
	end

  	def update
		@article=Article.find(params[:id])
		if 	@article.update_attributes(article_params)
			redirect_to admin_articles_path,:notice=>"Article updated"
		else
			render 'edit'
		end
	end
  
  	def index
    	@article=Article.all
  	end
  
  	def destroy
		@article=Article.find(params[:id])
		if @article.user_id==@current_user.id
			Comment.delete_all(["article_id=?" ,params[:id]])
			Article.delete(params[:id])
			redirect_to admin_articles_path, :notice => "Article deleted"
		else
			redirect_to admin_articles_path, :notice=>"Article doesn't belong to you"
		end
	end

  private

  def article_params
    	params.require(:article).permit(:title, :description, :rating, :user_id)
  end
end
