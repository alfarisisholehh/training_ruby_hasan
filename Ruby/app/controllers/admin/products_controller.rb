class Admin::ProductsController < Admin::ApplicationController
  before_filter :require_admin_login
  
  def index
    @product = Product.all
  end

  def new
    @product = Product.new
  end
  
  def show
    @product=Product.find(params[:id])
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      redirect_to admin_products_path,:notice=>"Product added"
    else
      render'new'
    end
  end

  def edit
    @product=Product.find(params[:id])
  end

  def update
    @product=Product.find(params[:id])
    if @product.update_attributes(product_params)
      redirect_to admin_products_path,:notice=>"Article updated"
    else
      render 'edit'
    end
  end

  def destroy
    @product=Product.find(params[:id])
    Product.delete(params[:id])
    redirect_to admin_products_path, :notice => "Article deleted"
  end

  private

  def product_params
    params.require(:product).permit(:name, :price, :stock, :description)
  end
end
