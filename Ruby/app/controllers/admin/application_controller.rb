class Admin::ApplicationController < ApplicationController
  
  protect_from_forgery
	before_filter :require_admin_login
  
  def require_admin_login
    if current_user.nil? || !current_user.admin?
      flash[:error] = "Only admins are permitted"
      redirect_to log_in_path
    else
      return current_user
    end
  end
end
