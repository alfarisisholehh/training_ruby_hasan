class SessionsController < ApplicationController
  def create
      @user = User.authenticate(params[:email], params[:password])
      if @user
        session[:user_id] = @user.id
        if @user.admin==1
          redirect_to admin_articles_path
        else
          redirect_to users_path, :notice => "You are Logged in!"
        end
      else
        flash.now.alert = "Invalid email or password" 
        redirect_to new_user_path
      end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
    flash.now.alert = "You are Logged Out!" 
  end  
end
