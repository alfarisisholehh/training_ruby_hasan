class UsersController < ApplicationController
before_filter :require_login, :only => [:index, :edit, :update, :delete]
 def index
    @user =User.all
  end
  
  def new
    @user =User.new
  end
 
  def create
    @user = User.new(user_params)
    if verify_recaptcha
      if @user.save
      #  UserMailer.registration_confirmation(@user).deliver
        redirect_to users_path,:notice=>"Sign Up"
      else
        render 'new'
      end  
    else
        flash[:error] = "There was an error with the recaptcha code below."
        render 'new'
    end      
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def edit
		@user=User.find(params[:id])
	end

  def update
		@user=User.find(params[:id])
		if @user.update_attributes(user_params)
			redirect_to users_path,:notice=>"Article updated"
		else
			render 'edit'
		end
	end

  def destroy
    @user=User.find(params[:id])
	  User.delete(params[:id])
		redirect_to users_path, :notice => "Article deleted"
  end

  private 

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :username, :password, :date_of_birth, :age, :address, :country_id)
  end 
end
