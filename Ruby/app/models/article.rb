class Article < ActiveRecord::Base
#	params :title,:description,:rating,:user_id

	belongs_to 	:user,
							:class_name => "User",
							:foreign_key => "user_id"
	has_many :comments, :dependent => :destroy
	scope :rating, lambda {|rating| where("rating >= ?", rating)}
	scope :more_100_chars, -> {where("length(description)>100")}

validates :title,
		      :presence =>true, 
		      :uniqueness=>true,
		      :allow_nil=>false,
          :allow_blank=>false
end
