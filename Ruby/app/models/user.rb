class User < ActiveRecord::Base
#   attr_accessible :email, :password, :password_confirmation, :age, :address
# 	Penting untuk ditanya coba tanya

	has_one :country
	has_many :articles, :dependent => :destroy
	has_many :products, :dependent => :destroy
	has_many :comments, :dependent => :destroy
	belongs_to :category
	belongs_to :product
  has_many :articles_my_country,
           :class_name => "Article",
           :conditions => "description like '%my_country%'"

	scope :age, -> {where("age >= 18")}
	scope :from, lambda {|country| where(country_id: country) }
	scope :indonesian, -> {where(country_id: Country.find_by_name("Indonesia").id)}

  def valid_name
    self.errors[:first_name] << "Can't filled by Martin"
  end

	def show_address
	   "Alamat: #{self.address} Negara: #{country.name}"
	end
	
	def self.show_age
	   "#{self.first_name} #{self.last_name}"
	end

	def encrypt_password
		if password.present?
			self.password_salt=BCrypt::Engine.generate_salt
			self.password_hash=BCrypt::Engine.hash_secret(password,password_salt)
		end
	end
	
	def self.authenticate(email,password)
		user=find_by_email(email)
		if user&&user.password_hash==BCrypt::Engine.hash_secret(password,user.password_salt)
			user
		else
			nil
		end
	end

#  validate :valid_name
	attr_accessor :password
	before_save :encrypt_password

	validates :password,
		:presence => {:on => :create},
		:confirmation => true

  	validates :first_name,
		:presence =>true, 
		:length=>{:minimum=>1,:maximum=>20},
#		:format=>{:with=> /[a-zA-Z\s]+$/},
		:uniqueness=>true,
		:allow_nil=>false		

#	validates :last_name,
#		:presence =>true, 
#		:length=>{:minimum=>1,:maximum=>20},
#		:format=>{:with=> /[a-zA-Z\s]+$/},
#		:allow_nil=>false
		
	validates :email,
		:presence=>true, 
		:length=>{:minimum => 3, :maximum => 254},
		:uniqueness=>true,
#		:format=>{:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i},
		:allow_nil=>false
end
