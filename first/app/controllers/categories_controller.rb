class CategoriesController < ApplicationController
  def index
    @category = Category.all
  end
  
  def new
    @category = Category.new
  end
  
  def create
     @category=Category.new(category_params)
	  if @category.save
			redirect_to categories_path,:notice=>"Article added"
		else
			render 'new'
		end  
  end

  def edit
    @category=Category.find(params[:id])
  end

    def update
    @category=Category.find(params[:id])
    if @category.update_attributes(category_params)
      redirect_to categories_path,:notice=>"Category updated"
    else
      render 'edit'
    end
  end

  def destroy
    @category=Category.find(params[:id])
    Category.delete(params[:id])
    redirect_to categories_path, :notice => "Category deleted"
  end

  private
  def category_params
    params.require(:category).permit(:name)
  end
end
