class Product < ActiveRecord::Base
  belongs_to :user
  has_many :category, :through => :user
  has_many :user
  scope :price, lambda {|price| where("price <= ?", price)}
  scope :stock, lambda {|stock| where("stock = ?", stock)}

validates :price,
		  :presence =>true, 
		  :allow_nil=>false,
          :numericality=>true
end
