class Category < ActiveRecord::Base
	has_many :products, :through => :user
	has_many :users
	belongs_to :product,
	     :class_name => "Product",
	     :foreign_key => "product_id"
	scope :category, lambda {|name| where("name = ?", name)}

  has_many :shoes_products,
        :class_name => "Product",
        :conditions => "name like '%Shoes%'"

validates :name,
		      :presence =>true, 
		      :uniqueness=>true,
		      :allow_nil=>false,
          :allow_blank=>false
end
