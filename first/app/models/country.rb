class Country < ActiveRecord::Base
  has_many :users
  has_many :indonesian_users,
           :class_name => "User",
           :conditions => "country_id = '6'"

	validates_inclusion_of :code, :in => %w(id ua frc)
end
