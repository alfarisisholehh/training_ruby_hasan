# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
@comments = Comments.find_by_id(1)
if @comments.nil?
  1.upto(5) do |i|
    comments = Comments.create({:content => "Content #{i}"})
  end
end

@users = User.find_by_username("hasan")
if @users.nil?
  1.upto(5) do |i|
   users = User.create({ :first_name => "Hasan #{i}", :last_name => "Farisi #{i}", :email => "hasan@hasan#{i}.com", :username => "hasan#{i}", :password => "hasan#{i}", :date_of_birth => "#{i}-#{i}-200#{i}", :age => "#{i}", :address => "Padalarang#{i}"})
  end
end

@articles = Article.find_by_id(1)
if @articles.nil?
  1.upto(5) do |i|
   articles = Article.create({:title => "Dummy Title #{i}", :body => "Dummy Body #{i}"})
  end
end

@countries = Countries.find_by_code(001)
if @countries.nil?
  1.upto(5) do |i|
   countries = Countries.create({:code => "00#{i}", :name => "Name 00#{i}"})
  end
end
