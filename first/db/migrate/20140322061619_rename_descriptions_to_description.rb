class RenameDescriptionsToDescription < ActiveRecord::Migration
  def change
    rename_column :articles, :descriptions, :description
  end
end
