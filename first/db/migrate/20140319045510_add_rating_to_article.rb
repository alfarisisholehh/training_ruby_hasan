class AddRatingToArticle < ActiveRecord::Migration
  def up
    add_column :articles, :rating, :integer
  end

  def down
    remove_column :article, :rating
  end
end
